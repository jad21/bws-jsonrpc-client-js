const ON_ERROR_MSG = ['Failed to fetch'];

export const sleep = (delay) => {
    return new Promise((resolve) => setTimeout(resolve, delay));
}

export const fetchRetry = (url, delay, tries, fetchOptions = {}, onErrorMsg = ON_ERROR_MSG) => {
    const onError = (err) => {
        if (onErrorMsg && onErrorMsg.length > 0 && !onErrorMsg.includes(err.message)) {
            throw err;
        }

        const triesLeft = tries - 1;
        if (!triesLeft || triesLeft < 0) {
            throw err;
        }

        if (!delay) {
            delay = 3000;
        }
        return sleep(delay).then(() => fetchRetry(url, delay, triesLeft, fetchOptions, onErrorMsg));
    }

    return fetch(url, fetchOptions).catch(onError);
}

export const param = (params) => {
    if (!params) return '';
    const fn = decodeURIComponent
    const queryString = Object.keys(params).map(key => `${fn(key)}=${fn(params[key])}`).join('&');

    return queryString;
}

export function paramToJson(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // set parameter name and value (use 'true' if empty)
            var paramName = a[0];
            var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

            // if the paramName ends with square brackets, e.g. colors[] or colors[2]
            if (paramName.match(/\[(\d+)?\]$/)) {

                // create key if it doesn't exist
                var key = paramName.replace(/\[(\d+)?\]/, '');
                if (!obj[key]) obj[key] = [];

                // if it's an indexed array e.g. colors[2]
                if (paramName.match(/\[\d+\]$/)) {
                    // get the index value and add the entry at the appropriate position
                    var index = /\[(\d+)\]/.exec(paramName)[1];
                    obj[key][index] = paramValue;
                } else {
                    // otherwise add the value to the end of the array
                    obj[key].push(paramValue);
                }
            } else {
                // we're dealing with a string
                if (!obj[paramName]) {
                    // if it doesn't exist, create property
                    obj[paramName] = paramValue;
                } else if (obj[paramName] && typeof obj[paramName] === 'string') {
                    // if property does exist and it's a string, convert it to an array
                    obj[paramName] = [obj[paramName]];
                    obj[paramName].push(paramValue);
                } else {
                    // otherwise add the property
                    obj[paramName].push(paramValue);
                }
            }
        }
    }

    return obj;
}

export const get = (url, params, headers = {}, retry = 0, retry_delay = 3000, onErrorMsg = ON_ERROR_MSG) => {
    let queryString = '';
    if (params) {
        queryString = '?' + param(params);
    }
    const fetchOptions = {
        headers: headers
    }
    return fetchRetry(`${url}${queryString}`, retry_delay, retry, fetchOptions, onErrorMsg)
}

export const post = (url, args = {}, retry = 0, retry_delay = 3000, onErrorMsg = ON_ERROR_MSG) => {
    const { params, body, json, headers } = args
    const _header = headers || { 'Content-Type': 'application/json;charset=utf-8' }
    let _body = body
    let _url = url
    if (params) {
        _url += '?' + param(params);
    }
    if (json) {
        _body = JSON.stringify(json);
    }
    const fetchOptions = {
        method: 'POST',
        headers: _header,
        body: _body
    }
    return fetchRetry(_url, retry_delay, retry, fetchOptions, onErrorMsg)
}

export const put = (url, args = {}, retry = 0, retry_delay = 3000, onErrorMsg = ON_ERROR_MSG) => {
    const { params, body, json, headers } = args
    const _header = headers || { 'Content-Type': 'application/json;charset=utf-8' }
    let _body = body
    let _url = url
    if (params) {
        _url += '?' + param(params);
    }
    if (json) {
        _body = JSON.stringify(json);
    }
    const fetchOptions = {
        method: 'PUT',
        headers: _header,
        body: _body
    }
    return fetchRetry(_url, retry_delay, retry, fetchOptions, onErrorMsg)
}

export const delete_ = (url, args = {}, retry = 0, retry_delay = 3000, onErrorMsg = ON_ERROR_MSG) => {
    const { params, body, json, headers } = args
    const _header = headers || { 'Content-Type': 'application/json;charset=utf-8' }
    let _body = body
    let _url = url
    if (params) {
        _url += '?' + param(params);
    }
    if (json) {
        _body = JSON.stringify(json);
    }
    const fetchOptions = {
        method: 'DELETE',
        headers: _header,
        body: _body
    }
    return fetchRetry(_url, retry_delay, retry, fetchOptions, onErrorMsg)
}

export default { get, post, put, delete_, sleep, fetchRetry };
