'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.paramToJson = paramToJson;
var ON_ERROR_MSG = ['Failed to fetch'];

var sleep = exports.sleep = function sleep(delay) {
    return new Promise(function (resolve) {
        return setTimeout(resolve, delay);
    });
};

var fetchRetry = exports.fetchRetry = function fetchRetry(url, delay, tries) {
    var fetchOptions = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
    var onErrorMsg = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : ON_ERROR_MSG;

    var onError = function onError(err) {
        if (onErrorMsg && onErrorMsg.length > 0 && !onErrorMsg.includes(err.message)) {
            throw err;
        }

        var triesLeft = tries - 1;
        if (!triesLeft || triesLeft < 0) {
            throw err;
        }

        if (!delay) {
            delay = 3000;
        }
        return sleep(delay).then(function () {
            return fetchRetry(url, delay, triesLeft, fetchOptions, onErrorMsg);
        });
    };

    return fetch(url, fetchOptions).catch(onError);
};

var param = exports.param = function param(params) {
    if (!params) return '';
    var fn = decodeURIComponent;
    var queryString = Object.keys(params).map(function (key) {
        return fn(key) + '=' + fn(params[key]);
    }).join('&');

    return queryString;
};

function paramToJson(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // set parameter name and value (use 'true' if empty)
            var paramName = a[0];
            var paramValue = typeof a[1] === 'undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

            // if the paramName ends with square brackets, e.g. colors[] or colors[2]
            if (paramName.match(/\[(\d+)?\]$/)) {

                // create key if it doesn't exist
                var key = paramName.replace(/\[(\d+)?\]/, '');
                if (!obj[key]) obj[key] = [];

                // if it's an indexed array e.g. colors[2]
                if (paramName.match(/\[\d+\]$/)) {
                    // get the index value and add the entry at the appropriate position
                    var index = /\[(\d+)\]/.exec(paramName)[1];
                    obj[key][index] = paramValue;
                } else {
                    // otherwise add the value to the end of the array
                    obj[key].push(paramValue);
                }
            } else {
                // we're dealing with a string
                if (!obj[paramName]) {
                    // if it doesn't exist, create property
                    obj[paramName] = paramValue;
                } else if (obj[paramName] && typeof obj[paramName] === 'string') {
                    // if property does exist and it's a string, convert it to an array
                    obj[paramName] = [obj[paramName]];
                    obj[paramName].push(paramValue);
                } else {
                    // otherwise add the property
                    obj[paramName].push(paramValue);
                }
            }
        }
    }

    return obj;
}

var get = exports.get = function get(url, params) {
    var headers = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    var retry = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
    var retry_delay = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 3000;
    var onErrorMsg = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : ON_ERROR_MSG;

    var queryString = '';
    if (params) {
        queryString = '?' + param(params);
    }
    var fetchOptions = {
        headers: headers
    };
    return fetchRetry('' + url + queryString, retry_delay, retry, fetchOptions, onErrorMsg);
};

var post = exports.post = function post(url) {
    var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var retry = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    var retry_delay = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 3000;
    var onErrorMsg = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : ON_ERROR_MSG;
    var params = args.params,
        body = args.body,
        json = args.json,
        headers = args.headers;

    var _header = headers || { 'Content-Type': 'application/json;charset=utf-8' };
    var _body = body;
    var _url = url;
    if (params) {
        _url += '?' + param(params);
    }
    if (json) {
        _body = JSON.stringify(json);
    }
    var fetchOptions = {
        method: 'POST',
        headers: _header,
        body: _body
    };
    return fetchRetry(_url, retry_delay, retry, fetchOptions, onErrorMsg);
};

var put = exports.put = function put(url) {
    var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var retry = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    var retry_delay = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 3000;
    var onErrorMsg = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : ON_ERROR_MSG;
    var params = args.params,
        body = args.body,
        json = args.json,
        headers = args.headers;

    var _header = headers || { 'Content-Type': 'application/json;charset=utf-8' };
    var _body = body;
    var _url = url;
    if (params) {
        _url += '?' + param(params);
    }
    if (json) {
        _body = JSON.stringify(json);
    }
    var fetchOptions = {
        method: 'PUT',
        headers: _header,
        body: _body
    };
    return fetchRetry(_url, retry_delay, retry, fetchOptions, onErrorMsg);
};

var delete_ = exports.delete_ = function delete_(url) {
    var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var retry = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    var retry_delay = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 3000;
    var onErrorMsg = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : ON_ERROR_MSG;
    var params = args.params,
        body = args.body,
        json = args.json,
        headers = args.headers;

    var _header = headers || { 'Content-Type': 'application/json;charset=utf-8' };
    var _body = body;
    var _url = url;
    if (params) {
        _url += '?' + param(params);
    }
    if (json) {
        _body = JSON.stringify(json);
    }
    var fetchOptions = {
        method: 'DELETE',
        headers: _header,
        body: _body
    };
    return fetchRetry(_url, retry_delay, retry, fetchOptions, onErrorMsg);
};

exports.default = { get: get, post: post, put: put, delete_: delete_, sleep: sleep, fetchRetry: fetchRetry };