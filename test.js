const { JsonRpcClient } = require("./dist");
const $rpc = JsonRpcClient("http://0.0.0.0:9094/jsonrpc")
// const proxy = require("./src/proxy");

// console.info(proxy)

// const rpc = proxy.createProxy(console.info)

// console.info(rpc.ping("pong"))

$rpc.ping().then(result => {
    console.info(result)
    // {"result": "pong"}
})


// function createProxy(action) {

//     // create the callable proxy
//     function _createCallableProxy(name) {
//         const methodNames = [name];

//         return new Proxy(function () { }, {
//             get(target, name, receiver) {
//                 // push a name of the method into the accumulator
//                 methodNames.push(name);

//                 return receiver;
//             },
//             apply(target, name, args) {
//                 // call the method finally
//                 return action(methodNames.join('.'), args);
//             }
//         });
//     }

//     // create the main proxy object
//     return new Proxy({}, {
//         get(target, name) {
//             return _createCallableProxy(name);
//         }
//     });
// }

// // create a proxy
// const remoteProxy = createProxy(function (methodName, args) {
//     console.log(`Calling a remote method "${methodName}" with arguments:`, args);
// });

// remoteProxy.long.path.to.method.to.call.on.remote.server(1, { actions: true });
// // -> Calling a remote method "long.path.to.method.to.call.on.remote.server" with arguments: [ 1, { actions: true } ]

// // the following won't affect the next `remoteProxy` call
// remoteProxy.huhu;
// remoteProxy.hehe.hihi;

// remoteProxy.next.remote.call({ name: 'Ondrej' });
// // -> Calling a remote method "next.remote.call" with arguments: [ { name: 'Ondrej' } ]

// const a = remoteProxy.method1;
// const b = remoteProxy.method2;

// a();
// // -> Calling a remote method "method1" with arguments: []