# JsonRpcClient JavaScript

## Instalacion
```bash
npm i bws-jsonrpc-client
```

## Instalacion desde gitlab
```json
{
    ...
    "dependencies": {
        "bws-jsonrpc-client": "https://gitlab.com/jad21/bws-jsonrpc-client-js@stable"
        ...
    }
    ...
}

```

## Modo de uso

```js

import JsonRpcClient from "bws-jsonrpc-client";

const $rpc = JsonRpcClient()

$rpc.ping().then(result => {
    console.info(result)
    // {"result": "pong"}
})

```
